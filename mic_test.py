import pyaudio

mic = pyaudio.PyAudio()
for idx in range(mic.get_device_count()):                   #On récupère le nombre total de device
        info = mic.get_device_info_by_index(idx)                #On récupère les infos de chaque device
        if "Kraken" in info["name"]:  #On cherche le micro de casque USB kraken
            print(info)