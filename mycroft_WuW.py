from precise_runner import PreciseEngine, PreciseRunner

engine = PreciseEngine('hey-computer/precise-engine/precise-engine', 'hey-computer/hey-computer.pb')
runner = PreciseRunner(engine, on_activation=lambda: print('hello'))
runner.start()

# Sleep forever
from time import sleep
while True:
    sleep(10)