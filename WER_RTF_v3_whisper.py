import whisper as w
import wave
import os
import subprocess
import pyaudio
import time
from threading import Thread
import openpyxl as o
from word2number import w2n
from num2words import num2words
import string
thread2=Thread()                                                                                                     #On initialise thread2 à un thread vide
base = "rostopic pub /mobile_base_controller/cmd_vel geometry_msgs/Twist -r 3 -- "                                   #La base de chaque commande de mouvement du robot
result = ""  
model = w.load_model("medium.en")
tot = "rostopic pub /mobile_base_controller/cmd_vel geometry_msgs/Twist -r 3 -- '[0.0,0.0,0.0]' '[0.0,0.0,0.0]'"     #La commande qui remet tout mouvement à 0
liste_commande = ["go_on","turn_left","turn_right","go_back","go_backward","go_left","go_right","go_for_1_second","go_during_1_second","go_for_1_minute","go_during_1_minute","go_for_1_centimeter","go_during_1_centimeter","go_for_1_meter","go_during_1_meter","go_faster","go_slower","go_ahead","go_in_front_of_you","go_a_bit_more","go_a_bit_further","go_a_lot_further","go_a_lot_more","change_the_speed_to_5","set_the_speed_to_5"]
liste_vrai_commande = ["go on","turn left","turn right","go back","go backward","go left","go right","go for 1 second","go for one second","go during 1 second","go during one second","go for 1 minute","go during 1 minute","go for 1 centimeter","go for 1 cm","go during 1 centimeter","go during 1 cm","go for 1 meter","go during 1 meter","go for one minute","go during one minute","go for one centimeter","go during one centimeter","go for one cm","go during one cm","go for one meter","go during one meter","go ahead","go in front of you","go a bit more","go a bit further","go a lot further","go a lot more"]
liste_vrai_complement = ["go faster","go slower","change the speed to 5","set the speed to 5","change the speed to five","set the speed to five"]

def numberxwords(test) :
    res = ""
    result =""
    for c in test : 
        if c.isdigit() or c == ".":
            res = res + c
            
        elif res != "" and c ==" ":
            res = num2words(float(res))
            result = result + res + " "
            
        elif type(c) == type('c'):
            result = result + c    
    if res.isdigit():         
        result = result + num2words(res)   
    else : 
        try :
            float(res)
            result = result + num2words(res)
        except ValueError:
            pass  
    
    return result     

def liste_commande_to_enregistrement(liste,nb):#Trouver nom tous les enregistrements en fonction du nombre de personne qui parle
    liste_enregistrement = {}
    for e in liste :
        for i in range(nb) :
            if "centimeter" in e :
                if numberxwords(e.replace("_", " ")) != "" : 
                    liste_enregistrement["~/Bureau/test_solution/mesures/enregistrements/" + str(i) + "/" + e + ".wav"] = [e.replace("_", " " ),numberxwords(e.replace("_", " ")),e.replace("_"," ").replace("centimeter","cm")]
                else :     
                    liste_enregistrement["~/Bureau/test_solution/mesures/enregistrements/" + str(i) + "/" + e + ".wav"] = [e.replace("_", " ",e.replace("_"," ").replace("centimeter","cm") )]
            else :
                if numberxwords(e.replace("_", " ")) != "" : 
                    liste_enregistrement["~/Bureau/test_solution/mesures/enregistrements/" + str(i) + "/" + e + ".wav"] = [e.replace("_", " " ),numberxwords(e.replace("_", " "))]
                else :     
                    liste_enregistrement["~/Bureau/test_solution/mesures/enregistrements/" + str(i) + "/" + e + ".wav"] = [e.replace("_", " " )]
                                                                                                                       
    return liste_enregistrement        

def find_name(mic):#Trouver le nom du micro                                                                     |
    for idx in range(mic.get_device_count()):                   #On récupère le nombre total de device
        info = mic.get_device_info_by_index(idx)                #On récupère les infos de chaque device
        if "Razer Kraken Ultimate: USB Audio" in info["name"]:  #On cherche le micro de casque USB kraken
            return info["name"][34:40]                          #On retourne le nom du device ("hw:qqc")
    print("ATTENTION : nom device pas trouvé")                  #On affiche une erreur si on ne trouve pas le casque souhaité

def find_num(res):
    try :
        num = w2n.word_to_num(res)
    except ValueError :
        num = ""
        start = False
        i = -1
        for c in res :
            i += 1
            if c.isdigit() or c == ".":
                num = num + c
                start = True
            elif c == "p" and res[i+1] == "o" and res[i+2] == "i" and res[i+3] == "n" and res[i+4] == "t" :
                num = "0." 
            elif start and c ==" ":
                break    
        
        if "." in num : 
            num = float(num)
        else :
            num= int(num)    
    return num

def verife_dico(s,mots):#Regarder si on connait au moins un des mots qui a été dit                              |
    l = s.split(' ')    #On sépare la phrase entendu à chauque espace pour récupérer une liste de mots
    for i in l :        #On parcourt la liste ainsi obtenue
        if i in mots :     #Si le mot de la liste fait partie de la liste de mot connu comme étant un morceau de commande
            return True #On renvoie True       
    return False        #Si non On renvoie false    

def comp(comp):#Retourner une valeur en fonction de la compréhension de ce qu'on a entendu                      |
    if comp == 0 :                                                                                              #pas compris
        os.system("export AUDIODEV="+device_name+"&&"+"export AUDIODRIVER=alsa"+"&&"+"play ~/Bureau/test_solution/son/pas_comp.wav -q")    #On joue le son voulu
        return 1                                                                                                #On renvoie 1 donc il faut écouter encore une fois
    elif comp == 1 :                                                                                            #pas tout compris   
        os.system("export AUDIODEV="+device_name+"&&"+"export AUDIODRIVER=alsa"+"&&"+"play ~/Bureau/test_solution/son/part_comp.wav -q")   #On joue le son voulu
        return 0                                                                                                #On renvoie 0 donc il faut continuer d'écouter
    elif comp == 2 :                                                                                            #compris
        return 2                                                                                                #On renvoie 2 donc il faut arrêter d'écouter
        
    else :                                                                                                      #S'il s'agit de la valeur de base de COMP et donc qu'on n'a encore rien entendu
        return 0                                                                                                #On renvoie 0 donc il faut continuer d'écouter

def find_id_vit(com):#Trouver où est la vitesse dans la commande et quelle est elle                             | a optimiser
    vit = 0                                         #On initialise la vitesse à 0
    id = -1                                         #On initialise la position de la vitesse à -1
    for i in com:                                   #On parcourt chaque caractère de la commande
        id = id + 1                                 #On avance la position d'une place
        if i == "[":                                #Si on trouve un "[" et donc qu'on rentre dans la partie de la commande qui renseigne les vitesses de chaque axe
            break                                   #On arrête de tout parcourir
    idx = id                                        #On donne a idx la valeur id, soit la valeur de début de la zone ou peut se trouver la vitesse
    id = -1                                         
    prevJ = ""  
    for j in com[idx:]:
        id = id +1
        if j == "-":
            vit = com[id+idx:id+idx+4]
            idC = id+idx
            break
        if j.isdigit() :
            if j != "0" :
                if  prevJ == "," or prevJ == "[":
                    vit = com[id+idx:id+idx+1]
                    idC = id+idx  
                    break
                elif prevJ == ".":
                    vit = com[id-2+idx:id+idx+1]
                    idC = id -2 + idx
                    break 
        prevJ = j
    if vit == 0:
        idC = 0       
    return idC,vit

def convertion(num):#trouver la valeur de retour d'un os.system                                                 |
    new_num=""                          #On initialise le nombre résultat que l'on cherche à une chaine de caractères vide
    for i in str(num) :                 #On parcourt toute la chaine de caractères donnée en entrée, soit le résultat de la commande os précédement éxécutée
        if i == "n" :                   #Si on trouve la lettre n, soit après un retour à la ligne donc s'il y a un nouveau nombre
            new_num = new_num + " "     #On ajoute un espace au nombre résultat      
        if ord(i) > 47 and ord(i) < 59: #Si après convertion, il s'agit bien d'un numéro 
            new_num = new_num + i       #On ajoute le numéro ainsi trouvé au nombre résultat
    return new_num                      #On renvoie le nombre résultat trouvé

#-Récupèrer commande petite ou partielle------------------------------------------------------------------------------------------------------------------------------------------------------------
def wait(result,command):#Attendre le temps voulu                                                               |
    print("wait")                                                                                   #On affiche "wait" pour indiquer le début de la fonction
    times = 0                                                                                       #On initialise le temps à 0
    vit = float(find_id_vit(command)[1])#en m/s                                                     #On trouve la vitesse avec la fonction find_id_vit    
    if vit != 0 :                                                                                   #Si on trouve effectivement une vitese non nul
        global COMP                                                                                 #On modifie COMP de façon global
        COMP = 2                                                                                    #On a comprit et on pourra effectuer une commande on donne donc à COMP la valeur 2
        if " centimeter" in result :                                                                #Si on trouve le mot "centimeter" dans ce qu'on a entendu
            dist = find_num(result)/100#en m                                                 #On trouve la distance demandée en convertissant la chaine de caractères de la distance que l'on a entendu
            times = dist / float(vit) #en s                                                         #On calcule le temps que va prendre le robot pour parcourir une telle distance avec t=d/v
        elif " meter" in result:                                                                    #Si on trouve le mot "meter" dans ce qu'on a entendu
            dist = find_num(result) #en m                                                    #On trouve la distance demandée en convertissant la chaine de caractères de la distance que l'on a entendu
            times = dist / float(vit) #en s                                                         #On calcule le temps que va prendre le robot pour parcourir une telle distance avec t=d/v
        
        elif " second" in result :                                                                  #Si on trouve le mot "second" dans ce qu'on a entendu
            times = find_num(result)#en s                                                    #On trouve le temps demandé en convertissant la chaine de caractères du temps que l'on a entendu
        elif " minute" in result :                                                                  #Si on trouve le mot "minute" dans ce qu'on a entendu
            times = find_num(result)*60 #en s                                                #On trouve le temps demandé en convertissant la chaine de caractères du temps que l'on a entendu

        elif " much" in result or " a lot" in result:                                               #Si on trouve le mot "much" ou "lot" dans ce qu'on a entendu, donc on veut avancer beaucoup
            times = 0.5/float(vit) #avancer de 50 cm                                                #On donne au temps la valeur necessaire pour avancer de 50cm en fonction de la vitesse
        elif " little" in result or " bit" in result:                                               #Si on trouve le mot "little" ou "bit" dans ce qu'on a entendu, donc on veut avancer un peu seulement
            times = 0.1/float(vit) #avancer de 10 cm                                                #On donne au temps la valeur necessaire pour avancer de 10cm en fonction de la vitesse

        else :                                                                                      #Si on ne trouve aucun de ces mots dans ce qu'on a entendu
              COMP = 1                                                                              #On donne 1 comme valeur à COMP car on a pas compris suffisement pour lancer une commande 
    
    if times != 0 :
        os.system("export AUDIODEV="+device_name+"&&"+"export AUDIODRIVER=alsa"+"&&"+"play ~/Bureau/test_solution/son/comp.wav -q")#On joue le son voulu 
        os.system("sleep "+str(times))                                                                #On attend le temps claculé précédement, on n'attend pas si aucun mot n' été entendu            
        quick_stop()                                                                                    #On stop le robot à la fin du temps ou si la commande n'est pas bien comprise, sans jouer le son de l'arret du robot
    else : 
        print("\033[0;91ml'unité n'a pas été entendu\033[0m")

def change_vit(com,mult):#Modifier la vitesse                                                                   |
    [idC,vit] = find_id_vit(com)            #On récupère la place de la vitesse ainsi que sa valeur grâce à la fonction find_id_vit
    vit = float(vit) + float(vit)*mult      #On donne à la vitesse sa nouvelle valeur, soit mult% sa valeur de base en plus
    comd = com[:idC]                        #On récupère la début de la commande jusqu'à l'emplacement de la vitesse
    comd = comd + str(vit) + tot[idC+3::]   #On ajoute la nouvelle vitesse et la fin de la commande
    return comd                             #On renvoie la nouvelle commande

def set_vit(com,mult):#Changer completement la vitesse                                                          |
    idC,vit = find_id_vit(com)                      #On récupère la place de la vitesse ainsi que sa valeur grâce à la fonction find_id_vit
    if float(vit) < 0.0 :                           #Si la vitesse est négative, donc que le robot recule
        comd = com[:idC] + "-"                      #On récupère la début de la commande jusqu'à l'emplacement de la vitesse et on ajoute un "-" pour garder une vitesse négative
    else :                                          #Si la vitesse est positive
        comd = com[:idC]                            #On récupère la début de la commande jusqu'à l'emplacement de la vitesse
    comd = comd + str(float(mult)) + tot[idC+3::]   #On donne à la nouvelle commande la valeur du début de la commande puis on ajoute la nouvelle vitesse et enfin on ajoute la fin de la commande
    return comd                                     #On renvoie la nouvelle commande

#-Récupèrer commande principale-------------------------------------------------------------------------------------------------------------------------------------------------------------------
def commander(result,last_command):#Commander le robot                                                          | 
    timed = False                                                                                                     #On initialise la variable "timed" a False, par défault la commande continue jusqu'à ce que l'utilisateur dise "stop"
    global commande                                                                                                   #On modifie la valeur global de la variable commande
    print("commander")                                                                                                #On affiche "commander" pour indiquer le début de la fonction 
    print("last com",last_command)
    dif = False                                                                                                       #On initialise la variable "dif" a False, rien n'a été modifié     
    if " further" in result or " more" in result :                                                                    #Si on a entendu le mot "further" ou "more"
        thread_time = Thread(target=wait, args=[result,commande])                                                     #On initialise le thread pour attendre le bon temps durant lequel la commande s'exécute
        thread_time.start()                                                                                           #On lance le thread 
        os.system("cd /tiago_public_ws")                                                                              #On retourne dans le bon repertoire si besoin
        os.system(last_command)                                                                                       #On execute la dernière commande éxécutée commande
        global COMP                                                                                                   #On mondifie la valeur global de COMP 
        COMP = 2                                                                                                      #On donne a COMP la valeur 2, on a compris la commande
    
    else :                                                                                                            #Si on a pas utilisé la fonction wait 
        if "during" in result or " for " in result:                                                                   #Si on a entendu le mot "during" ou "for", l'action est a faire pendant un certain temps / distance
            timed = True                                                                                              #On donne a timed la valeur True

        if "turn" in result :                                                                                         #Si on a entendu le mot "turn"
            if "right" in  result :                                                                                   #Si on a entendu le mot "right"
                commande = base + "'[0.0,0.0,0.0]' '[0.0, 0.0, -0.3]'"                                                #On ajoute la fin de la commande pour tourner à droite à la commande de base
                dif = True                                                                                            #On donne a dif la valeur True, la commande a été modifiée
            elif "left" in  result :                                                                                  #Si on a entendu le mot "left"
                commande = base + "'[0.0,0.0,0.0]' '[0.0, 0.0, 0.3]'"                                                 #On ajoute la fin de la commande pour tourner à gauche à la commande de base
                dif = True                                                                                            #On donne a dif la valeur True, la commande a été modifiée

        elif "go" in result :                                                                                         #Si on a entendu le mot "go"                
            if "back" in  result or "backward" in result:                                                             #Si on a entendu le mot "back" ou "backward"
                commande = base + "'[-0.5,0.0,0.0]' '[0.0, 0.0, 0.0]'"                                                #On ajoute la fin de la commande pour aller en arrière à la commande de base
                dif = True                                                                                            #On donne a dif la valeur True, la commande a été modifiée
            elif "right" in  result :                                                                                 #Si on a entendu le mot "right"
                commande = base + "'[0.0,0.5,0.0]' '[0.0, 0.0, 0.0]'"                                                 #On ajoute la fin de la commande pour aller à droite à la commande de base
                dif = True                                                                                            #On donne a dif la valeur True, la commande a été modifiée
            elif "left" in  result :                                                                                  #Si on a entendu le mot "left"
                commande = base + "'[0.0,-0.5,0.0]' '[0.0, 0.0, 0.0]'"                                                #On ajoute la fin de la commande pour aller à gauche à la commande de base
                dif = True                                                                                            #On donne a dif la valeur True, la commande a été modifiée
            else :
                commande = base + "'[0.5,0.0,0.0]' '[0.0, 0.0, 0.0]'"                                                 #On ajoute la fin de la commande pour aller tout droit à la commande de base
                dif = True                                                                                            #On donne a dif la valeur True, la commande a été modifiée    

        if dif == True:                                                                                               #Si la commande a été modifiée 
            quick_stop()                                                                                              #On appele la fonction quick_stop pour être sur que le robot n'est pas déjà entrain d'avancer au moment où on lance la commande 
            os.system("export AUDIODEV="+device_name+"&&"+"export AUDIODRIVER=alsa"+"&&"+"play ~/Bureau/test_solution/son/comp.wav -q")          #On joue le son voulu   
            os.system("cd /tiago_public_ws")                                                                          #On retourne dans le bon repertoire si besoin
            if timed :                                                                                                #Si il faut éxécuter la commande pendant un temps donné
                print("timed")                                                                                        #On affiche "timed" pour indiquer que la commande est timé
                thread_time = Thread(target=wait, args=[result,commande])                                             #On initialise le thread pour attendre le bon temps durant lequel la commande s'exécute
                thread_time.start()                                                                                   #On lance le thread 
                os.system("cd /tiago_public_ws")                                                                      #On retourne dans le bon repertoire si besoin
                os.system(last_command)                                                                               #On execute la dernière commande éxécutée commande
            else :                                                                                                    #Si il faut exécuter la commande jusqu'à ce que l"utilisateur dise "stop"
                print(commande)                                                                                       #On affiche la commande
                os.system(commande)                                                                                   #On execute la commande
                
            if  not thread2.is_alive() and not timed:                                                                 #Si la fonction complément n'est pas lancé et que la fonction wait n'a pas déjà donné une valeur à COMP
                COMP = 2                                                                                              #On donne a COMP la valeur 2, on a compris la commande
                print("COMP 4 : ",COMP)

        else :                                                                                                        #Si la valeur de dif est toujours sa valeur d'initialisation, la commande n'a pas été modifiée
            print("\033[0;91maucune commande n'est lancée\033[0m")                                                    #On affiche un message signifiant que aucune commande n'a été lancée
        print("fin")                                                                                                  #On affiche "fin" pour indiquer la fin de la fonction

def complement (result,command):#Commander le robot pendant qu il effectue déjà une action                      | 
    print("complement")                                                                                     #On affiche "complement" pour indiquer le début de la fonction  
    global commande                                                                                         #On modifie la commande global
    if "faster" in result :                                                                               #si on a entendu le mot "faster"
        commande = change_vit(command,0.5)                                                                  #On lance la fonction change_vit pour modifier la vitesse actuelle
    elif "slower" in result :                                                                               #si on a entendu le mot "slower"
        commande = change_vit(command,-0.5)                                                                 #On lance la fonction change_vit pour modifier la vitesse actuelle  
    elif "Change" in result or "Set" in result :                                                            #si on a entendu le mot "change" ou "set"
        if "speed" in result :                                                                              #si on a entendu le mot "speed", on pourra ajouter d'autre if plus tard s'il faut modifier d'autres variables
            multi = find_num(result)                                                                 #On récupère la valeur de la vitesse entendu
            commande = set_vit(command,multi)                                                               #On lance la fonction set_vit pour changer complètement la vitesse actuelle

    if commande != "":                                                                                      #On vérifie que la commande modifié n'est pas vide 
        global COMP                                                                                         #On mondifie la valeur global de COMP 
        COMP = 1                                                                                            #On donne a COMP la valeur 1, on a compris la commande mais il ne faut pas mettre à 2 pour ne pas stopper l'écoute
        os.system("export AUDIODEV="+device_name+"&&"+"export AUDIODRIVER=alsa"+"&&"+"play ~/Bureau/test_solution/son/comp.wav -q")    #On joue le son voulu
        os.system("cd /tiago_public_ws")                                                                    #On retourne dans le bon repertoire si besoin
        print(commande)                                                                                     #On affiche la commande modifiée
        quick_stop()                                                                                        #On appele la fonction quick_stop pour être sur que le robot n'est pas déjà entrain d'avancer au moment où on lance la commande 
        os.system(commande)                                                                                 #On execute la commande
    else :                                                                                                  #Si la commande modifiée a toujours pour valeur sa valeur d'initialisation
        print("\033[0;91mla commande n'est pas modifiée\033[0m")                                            #On affiche un message signifiant que la commande n'a pas été modifiée

#-Stop--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
def quick_stop() :#Arreter le robot plus rapidement sans faire de bruit                                         |
    print("quick stop")                                                                                     #On affiche "stop" pour indiquer le début de la fonction  
    num = subprocess.check_output("ps aux | grep rostopic | cut -b 10-16", shell=True)                      #On cherche le numero du processus qui correspond au mouvement du robot
    num = convertion(num)                                                                                   #On modifie le resultat trouvé par le commande ci dessus pour avoir un nombre utilisable
    comm = "kill " + str(num)                                                                               #On créer la commande pour tuer le processus voulu
    os.system(comm)                                                                                         #On execute cette commande

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

def wer(ref, hyp ,debug=True):
    r = ref.split()
    h = hyp.split()
    #costs will holds the costs, like in the Levenshtein distance algorithm
    costs = [[0 for inner in range(len(h)+1)] for outer in range(len(r)+1)]
    # backtrace will hold the operations we've done.
    # so we could later backtrace, like the WER algorithm requires us to.
    backtrace = [[0 for inner in range(len(h)+1)] for outer in range(len(r)+1)]
 
    OP_OK = 0
    OP_SUB = 1
    OP_INS = 2
    OP_DEL = 3
    DEL_PENALTY = 1
    INS_PENALTY = 1
    SUB_PENALTY = 1
    
    # First column represents the case where we achieve zero
    # hypothesis words by deleting all reference words.
    for i in range(1, len(r)+1):
        costs[i][0] = DEL_PENALTY*i
        backtrace[i][0] = OP_DEL
    
    # First row represents the case where we achieve the hypothesis
    # by inserting all hypothesis words into a zero-length reference.
    for j in range(1, len(h) + 1):
        costs[0][j] = INS_PENALTY * j
        backtrace[0][j] = OP_INS
    
    # computation
    for i in range(1, len(r)+1):
        for j in range(1, len(h)+1):
            if r[i-1] == h[j-1]:
                costs[i][j] = costs[i-1][j-1]
                backtrace[i][j] = OP_OK
            else:
                substitutionCost = costs[i-1][j-1] + SUB_PENALTY # penalty is always 1
                insertionCost    = costs[i][j-1] + INS_PENALTY   # penalty is always 1
                deletionCost     = costs[i-1][j] + DEL_PENALTY   # penalty is always 1
                 
                costs[i][j] = min(substitutionCost, insertionCost, deletionCost)
                if costs[i][j] == substitutionCost:
                    backtrace[i][j] = OP_SUB
                elif costs[i][j] == insertionCost:
                    backtrace[i][j] = OP_INS
                else:
                    backtrace[i][j] = OP_DEL
                 
    # back trace though the best route:
    i = len(r)
    j = len(h)
    numSub = 0
    numDel = 0
    numIns = 0
    numCor = 0
    if debug:
        #print("OP\tREF\tHYP")
        lines = []
    while i > 0 or j > 0:
        if backtrace[i][j] == OP_OK:
            numCor += 1
            i-=1
            j-=1
            if debug:
                lines.append("OK\t" + r[i]+"\t"+h[j])
        elif backtrace[i][j] == OP_SUB:
            numSub +=1
            i-=1
            j-=1
            if debug:
                lines.append("SUB\t" + r[i]+"\t"+h[j])
        elif backtrace[i][j] == OP_INS:
            numIns += 1
            j-=1
            if debug:
                lines.append("INS\t" + "****" + "\t" + h[j])
        elif backtrace[i][j] == OP_DEL:
            numDel += 1
            i-=1
            if debug:
                lines.append("DEL\t" + r[i]+"\t"+"****")
    if debug:
        lines = reversed(lines)
        #for line in lines:
            #print(line)
        #print("#cor " + str(numCor))
        #print("#sub " + str(numSub))
        #print("#del " + str(numDel))
        #print("#ins " + str(numIns))
    # return (numSub + numDel + numIns) / (float) (len(r))
    wer_result = round( (numSub + numDel + numIns) / (float) (len(r)), 3)
    return {'WER':wer_result, 'numCor':numCor, 'numSub':numSub, 'numIns':numIns, 'numDel':numDel, "numCount": len(r)}

device_name = find_name(pyaudio.PyAudio())

dic = liste_commande_to_enregistrement(liste_commande,2)
list = list(dic.keys())
for j in list :
    result = ""
    audio_file = os.path.expanduser(j)
    
    i=1
    while i <= 5 :
        faux = 0
        bon = 0
        okok = False
        result = ""
        last_commande = "rostopic pub /mobile_base_controller/cmd_vel geometry_msgs/Twist -r 3 -- '[0.5,0.0,0.0]' '[0.0,0.0,0.0]'"
        start = time.time()
        
        with wave.open(audio_file, "rb") as wav_file:
            # Get audio file properties
            channels = wav_file.getnchannels()
            sample_width = wav_file.getsampwidth()
            frame_rate = wav_file.getframerate()
            frames = wav_file.getnframes()
            duration = frames / float(frame_rate)
            
        result = model.transcribe(audio_file)['text'][1:].lower()
        if result[-1:] in string.punctuation :
            result = model.transcribe(audio_file)['text'][1:-1].lower()
        print("\nRecognition result:",result )
        mid = time.time()
        
        if result in liste_vrai_commande :
            print("\033[0;92mbon\033[0m")
            bon = 1
            i += 1
            commander(result,last_commande)
            end = time.time()
            last_commande = commande
            okok = True

        elif result in liste_vrai_complement :  
            print("\033[0;92mbon\033[0m")
            bon = 1
            i += 1  
            complement(result, last_commande)
            last_commande = commande 
            end = time.time()
            okok = True

        else :
            print("\033[0;92mfaux\033[0m")
            faux = 1
            i += 1
            okok = True
            end=time.time()

        wer_value = 1000
        for k in dic[j] :
                wer_value_temp = (wer(k,result)['WER'])
                if wer_value_temp < wer_value : 
                    wer_value = wer_value_temp
                #print(wer_value)    

        if okok : 
            print("\033[0;91mtrying to load\033[0m")
            try:
                # Try to load the existing workbook
                workbook = o.load_workbook('WER_RTF_whisper.xlsx')
            except FileNotFoundError:
                print("\033[0;91mcreating xlsx\033[0m")
                # If the file doesn't exist, create a new workbook
                workbook = o.Workbook()
                # Save the new workbook to create the file
                workbook.save('WER_RTF_whisper.xlsx')
            
            # Create a new sheet
            print("\033[0;91mcreating new sheet\033[0m")
            new_sheet = workbook.create_sheet('Mesure'+str(len(workbook.sheetnames)),len(workbook.sheetnames))
            
            if end != 0 :
                # Write data to specific cells in the new sheet
                new_sheet['A1'] = 'ecoute'
                new_sheet['A2'] = 'decodage'
                print("\033[0;95m"+"start\t--\t"+str(start)+" \nmid\n--\n"+str(mid)+"\nend\n-- "+str(end)+"\033[0m")
                new_sheet['B1'] = mid - start + duration
                new_sheet['B2'] = end - mid
            print("\033[0;95m"+"bon\n--\n"+str(bon)+" \nmauvais\n--\n"+str(faux)+"\033[0m")
            new_sheet['A4'] = 'bon'
            new_sheet['A5'] = 'mauvais'
            new_sheet['B4'] = bon
            new_sheet['B5'] = faux

            new_sheet['D4'] = 'WER'
            new_sheet['D5'] = wer_value
            if wer_value != 0 :
                print(dic[j])
                print(wer_value)
                new_sheet['D6'] = 1

            new_sheet['I1'] = j    
            # Save the changes
            print("\033[0;91mWER_RTF saved\033[0m")
            workbook.save('WER_RTF_whisper.xlsx')


