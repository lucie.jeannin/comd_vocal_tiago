from xml.dom.minidom import TypeInfo
from vosk import Model, KaldiRecognizer
import pyaudio 
import os
from threading import Thread
import subprocess 
from word2number import w2n
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
C = {"go", "back", "turn", "left", "right", "backward","further","more","for","during"}                              #Le vocaublaire utilisé pour les commandes
C2 = {"change","speed","faster","slower","set"}                                                                      #Le vocaublaire utilisé pour modifier les commandes
global WW
WW = ""                                                                                                              #Le wake up word entendu
global COMP
COMP = -1                                                                                                            #Savoir si le programme à compris (0 pour non 1 pour partiellement et 2 pour oui et -1 pour valeur de depart de COMP)
SAMPLE_RATE = 16000
global commande
commande = ""                                                                                                        #La commande qui va être executées pour bouger le robot 
base = "rostopic pub /mobile_base_controller/cmd_vel geometry_msgs/Twist -r 3 -- "                                   #La base de chaque commande de mouvement du robot
tot = "rostopic pub /mobile_base_controller/cmd_vel geometry_msgs/Twist -r 3 -- '[0.0,0.0,0.0]' '[0.0,0.0,0.0]'"     #La commande qui remet tout mouvement à 0
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-Chercher infos----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
def find_name(mic):#Trouver le nom du micro                                                                     |
    for idx in range(mic.get_device_count()):                   #On récupère le nombre total de device
        info = mic.get_device_info_by_index(idx)                #On récupère les infos de chaque device
        if "Razer Kraken Ultimate: USB Audio" in info["name"]:  #On cherche le micro de casque USB kraken
            return info["name"][34:40]                          #On retourne le nom du device ("hw:qqc")
    print("\033[0;91mATTENTION : nom device pas trouvé\033[0m")                  #On affiche une erreur si on ne trouve pas le casque souhaité

def verife_dico(s,mots):#Regarder si on connait au moins un des mots qui a été dit                              |
    l = s.split(' ')    #On sépare la phrase entendu à chauque espace pour récupérer une liste de mots
    for i in l :        #On parcourt la liste ainsi obtenue
        if i in mots :     #Si le mot de la liste fait partie de la liste de mot connu comme étant un morceau de commande
            return True #On renvoie True       
    return False        #Si non On renvoie false    

def comp(comp):#Retourner une valeur en fonction de la compréhension de ce qu'on a entendu                      |
    if comp == 0 :                                                                                              #pas compris
        os.system("export AUDIODEV="+device_name+"&&"+"export AUDIODRIVER=alsa"+"&&"+"play ./son/pas_comp.wav -q")    #On joue le son voulu
        return 1                                                                                                #On renvoie 1 donc il faut écouter encore une fois
    elif comp == 1 :                                                                                            #pas tout compris   
        os.system("export AUDIODEV="+device_name+"&&"+"export AUDIODRIVER=alsa"+"&&"+"play ./son/part_comp.wav -q")   #On joue le son voulu
        return 0                                                                                                #On renvoie 0 donc il faut continuer d'écouter
    elif comp == 2 :                                                                                            #compris
        return 2                                                                                                #On renvoie 2 donc il faut arrêter d'écouter
        
    else :                                                                                                      #S'il s'agit de la valeur de base de COMP et donc qu'on n'a encore rien entendu
        return 0                                                                                                #On renvoie 0 donc il faut continuer d'écouter

def find_id_vit(com):#Trouver où est la vitesse dans la commande et quelle est elle                             | a optimiser
    vit = 0                                         #On initialise la vitesse à 0
    id = -1                                         #On initialise la position de la vitesse à -1
    for i in com:                                   #On parcourt chaque caractère de la commande
        id = id + 1                                 #On avance la position d'une place
        if i == "[":                                #Si on trouve un "[" et donc qu'on rentre dans la partie de la commande qui renseigne les vitesses de chaque axe
            break                                   #On arrête de tout parcourir
    idx = id                                        #On donne a idx la valeur id, soit la valeur de début de la zone ou peut se trouver la vitesse
    id = -1                                         
    prevJ = ""  
    for j in com[idx:]:
        id = id +1
        if j == "-":
            vit = com[id+idx:id+idx+4]
            idC = id+idx
            break
        if j.isdigit() :
            if j != "0" :
                if  prevJ == "," or prevJ == "[":
                    vit = com[id+idx:id+idx+1]
                    idC = id+idx  
                    break
                elif prevJ == ".":
                    vit = com[id-2+idx:id+idx+1]
                    idC = id -2 + idx
                    break 
        prevJ = j
    if vit == 0:
        idC = 0       
    return idC,vit

def convertion(num):#trouver la valeur de retour d'un os.system                                                 |
    new_num=""                          #On initialise le nombre résultat que l'on cherche à une chaine de caractères vide
    for i in str(num) :                 #On parcourt toute la chaine de caractères donnée en entrée, soit le résultat de la commande os précédement éxécutée
        if i == "n" :                   #Si on trouve la lettre n, soit après un retour à la ligne donc s'il y a un nouveau nombre
            new_num = new_num + " "     #On ajoute un espace au nombre résultat      
        if ord(i) > 47 and ord(i) < 59: #Si après convertion, il s'agit bien d'un numéro 
            new_num = new_num + i       #On ajoute le numéro ainsi trouvé au nombre résultat
    return new_num                      #On renvoie le nombre résultat trouvé

#-Récupèrer commande petite ou partielle------------------------------------------------------------------------------------------------------------------------------------------------------------
def wait(result,command):#Attendre le temps voulu                                                               |
    print("wait")                                                                                   #On affiche "wait" pour indiquer le début de la fonction
    times = 0                                                                                       #On initialise le temps à 0
    vit = float(find_id_vit(command)[1])#en m/s                                                     #On trouve la vitesse avec la fonction find_id_vit    
    if vit != 0 :                                                                                   #Si on trouve effectivement une vitese non nul
        global COMP                                                                                 #On modifie COMP de façon global
        COMP = 2                                                                                    #On a comprit et on pourra effectuer une commande on donne donc à COMP la valeur 2
        if " centimeter" in result :                                                                #Si on trouve le mot "centimeter" dans ce qu'on a entendu
            dist = w2n.word_to_num(result)/100#en m                                                 #On trouve la distance demandée en convertissant la chaine de caractères de la distance que l'on a entendu
            times = dist / float(vit) #en s                                                         #On calcule le temps que va prendre le robot pour parcourir une telle distance avec t=d/v
        elif " meter" in result:                                                                    #Si on trouve le mot "meter" dans ce qu'on a entendu
            dist = w2n.word_to_num(result) #en m                                                    #On trouve la distance demandée en convertissant la chaine de caractères de la distance que l'on a entendu
            times = dist / float(vit) #en s                                                         #On calcule le temps que va prendre le robot pour parcourir une telle distance avec t=d/v
        
        elif " second" in result :                                                                  #Si on trouve le mot "second" dans ce qu'on a entendu
            times = w2n.word_to_num(result)#en s                                                    #On trouve le temps demandé en convertissant la chaine de caractères du temps que l'on a entendu
        elif " minute" in result :                                                                  #Si on trouve le mot "minute" dans ce qu'on a entendu
            times = w2n.word_to_num(result)*60 #en s                                                #On trouve le temps demandé en convertissant la chaine de caractères du temps que l'on a entendu

        elif " much" in result or " a lot" in result:                                               #Si on trouve le mot "much" ou "lot" dans ce qu'on a entendu, donc on veut avancer beaucoup
            times = 0.5/float(vit) #avancer de 50 cm                                                #On donne au temps la valeur necessaire pour avancer de 50cm en fonction de la vitesse
        elif " little" in result or " bit" in result:                                               #Si on trouve le mot "little" ou "bit" dans ce qu'on a entendu, donc on veut avancer un peu seulement
            times = 0.1/float(vit) #avancer de 10 cm                                                #On donne au temps la valeur necessaire pour avancer de 10cm en fonction de la vitesse

        else :                                                                                      #Si on ne trouve aucun de ces mots dans ce qu'on a entendu
              COMP = 1                                                                              #On donne 1 comme valeur à COMP car on a pas compris suffisement pour lancer une commande 
    
    if times != 0 :
        os.system("export AUDIODEV="+device_name+"&&"+"export AUDIODRIVER=alsa"+"&&"+"play ./son/comp.wav -q")#On joue le son voulu 
        os.system("sleep "+str(times))                                                                #On attend le temps claculé précédement, on n'attend pas si aucun mot n' été entendu            
        quick_stop()                                                                                    #On stop le robot à la fin du temps ou si la commande n'est pas bien comprise, sans jouer le son de l'arret du robot
    else : 
        print("\033[0;91ml'unité n'a pas été entendu\033[0m")

def change_vit(com,mult):#Modifier la vitesse                                                                   |
    [idC,vit] = find_id_vit(com)            #On récupère la place de la vitesse ainsi que sa valeur grâce à la fonction find_id_vit
    vit = float(vit) + float(vit)*mult      #On donne à la vitesse sa nouvelle valeur, soit mult% sa valeur de base en plus
    comd = com[:idC]                        #On récupère la début de la commande jusqu'à l'emplacement de la vitesse
    comd = comd + str(vit) + tot[idC+3::]   #On ajoute la nouvelle vitesse et la fin de la commande
    return comd                             #On renvoie la nouvelle commande

def set_vit(com,mult):#Changer completement la vitesse                                                          |
    idC,vit = find_id_vit(com)                      #On récupère la place de la vitesse ainsi que sa valeur grâce à la fonction find_id_vit
    if float(vit) < 0.0 :                           #Si la vitesse est négative, donc que le robot recule
        comd = com[:idC] + "-"                      #On récupère la début de la commande jusqu'à l'emplacement de la vitesse et on ajoute un "-" pour garder une vitesse négative
    else :                                          #Si la vitesse est positive
        comd = com[:idC]                            #On récupère la début de la commande jusqu'à l'emplacement de la vitesse
    comd = comd + str(float(mult)) + tot[idC+3::]   #On donne à la nouvelle commande la valeur du début de la commande puis on ajoute la nouvelle vitesse et enfin on ajoute la fin de la commande
    return comd                                     #On renvoie la nouvelle commande

#-Récupèrer commande principale-------------------------------------------------------------------------------------------------------------------------------------------------------------------
def commander(result,last_command):#Commander le robot                                                          | 
    timed = False                                                                                                     #On initialise la variable "timed" a False, par défault la commande continue jusqu'à ce que l'utilisateur dise "stop"
    global commande                                                                                                   #On modifie la valeur global de la variable commande
    print("commander")                                                                                                #On affiche "commander" pour indiquer le début de la fonction 
    print("last com",last_command)
    dif = False                                                                                                       #On initialise la variable "dif" a False, rien n'a été modifié     
    if " further" in result or " more" in result :                                                                    #Si on a entendu le mot "further" ou "more"
        thread_time = Thread(target=wait, args=[result,commande])                                                     #On initialise le thread pour attendre le bon temps durant lequel la commande s'exécute
        thread_time.start()                                                                                           #On lance le thread 
        os.system("cd /tiago_public_ws")                                                                              #On retourne dans le bon repertoire si besoin
        os.system(last_command)                                                                                       #On execute la dernière commande éxécutée commande
        global COMP                                                                                                   #On mondifie la valeur global de COMP 
        COMP = 2                                                                                                      #On donne a COMP la valeur 2, on a compris la commande
    
    else :                                                                                                            #Si on a pas utilisé la fonction wait 
        if "during" in result or " for " in result:                                                                   #Si on a entendu le mot "during" ou "for", l'action est a faire pendant un certain temps / distance
            timed = True                                                                                              #On donne a timed la valeur True

        if "turn" in result :                                                                                         #Si on a entendu le mot "turn"
            if "right" in  result :                                                                                   #Si on a entendu le mot "right"
                commande = base + "'[0.0,0.0,0.0]' '[0.0, 0.0, -0.3]'"                                                #On ajoute la fin de la commande pour tourner à droite à la commande de base
                dif = True                                                                                            #On donne a dif la valeur True, la commande a été modifiée
            elif "left" in  result :                                                                                  #Si on a entendu le mot "left"
                commande = base + "'[0.0,0.0,0.0]' '[0.0, 0.0, 0.3]'"                                                 #On ajoute la fin de la commande pour tourner à gauche à la commande de base
                dif = True                                                                                            #On donne a dif la valeur True, la commande a été modifiée

        elif "go" in result :                                                                                         #Si on a entendu le mot "go"                
            if "back" in  result or "backward" in result:                                                             #Si on a entendu le mot "back" ou "backward"
                commande = base + "'[-0.5,0.0,0.0]' '[0.0, 0.0, 0.0]'"                                                #On ajoute la fin de la commande pour aller en arrière à la commande de base
                dif = True                                                                                            #On donne a dif la valeur True, la commande a été modifiée
            elif "right" in  result :                                                                                 #Si on a entendu le mot "right"
                commande = base + "'[0.0,0.5,0.0]' '[0.0, 0.0, 0.0]'"                                                 #On ajoute la fin de la commande pour aller à droite à la commande de base
                dif = True                                                                                            #On donne a dif la valeur True, la commande a été modifiée
            elif "left" in  result :                                                                                  #Si on a entendu le mot "left"
                commande = base + "'[0.0,-0.5,0.0]' '[0.0, 0.0, 0.0]'"                                                #On ajoute la fin de la commande pour aller à gauche à la commande de base
                dif = True                                                                                            #On donne a dif la valeur True, la commande a été modifiée
            else :
                commande = base + "'[0.5,0.0,0.0]' '[0.0, 0.0, 0.0]'"                                                 #On ajoute la fin de la commande pour aller tout droit à la commande de base
                dif = True                                                                                            #On donne a dif la valeur True, la commande a été modifiée    

        if dif == True:                                                                                               #Si la commande a été modifiée 
            quick_stop()                                                                                              #On appele la fonction quick_stop pour être sur que le robot n'est pas déjà entrain d'avancer au moment où on lance la commande 
            os.system("export AUDIODEV="+device_name+"&&"+"export AUDIODRIVER=alsa"+"&&"+"play ./son/comp.wav -q")          #On joue le son voulu   
            os.system("cd /tiago_public_ws")                                                                          #On retourne dans le bon repertoire si besoin
            if timed :                                                                                                #Si il faut éxécuter la commande pendant un temps donné
                print("timed")                                                                                        #On affiche "timed" pour indiquer que la commande est timé
                thread_time = Thread(target=wait, args=[result,commande])                                             #On initialise le thread pour attendre le bon temps durant lequel la commande s'exécute
                thread_time.start()                                                                                   #On lance le thread 
                os.system("cd /tiago_public_ws")                                                                      #On retourne dans le bon repertoire si besoin
                os.system(last_command)                                                                               #On execute la dernière commande éxécutée commande
            else :                                                                                                    #Si il faut exécuter la commande jusqu'à ce que l"utilisateur dise "stop"
                print(commande)                                                                                       #On affiche la commande
                os.system(commande)                                                                                   #On execute la commande
                
            if  not thread2.is_alive() and not timed:                                                                 #Si la fonction complément n'est pas lancé et que la fonction wait n'a pas déjà donné une valeur à COMP
                COMP = 2                                                                                              #On donne a COMP la valeur 2, on a compris la commande
                print("COMP 4 : ",COMP)

        else :                                                                                                        #Si la valeur de dif est toujours sa valeur d'initialisation, la commande n'a pas été modifiée
            print("\033[0;91maucune commande n'est lancée\033[0m")                                                    #On affiche un message signifiant que aucune commande n'a été lancée
        print("fin")                                                                                                  #On affiche "fin" pour indiquer la fin de la fonction

def complement (result,command):#Commander le robot pendant qu il effectue déjà une action                      | 
    print("complement")                                                                                     #On affiche "complement" pour indiquer le début de la fonction  
    global commande                                                                                         #On modifie la commande global
    if "stop" in result :                                                                                   #si on a entendu le mot "stop"
        stop()                                                                                              #On lance la fonction stop
    elif "faster" in result :                                                                               #si on a entendu le mot "faster"
        commande = change_vit(command,0.5)                                                                  #On lance la fonction change_vit pour modifier la vitesse actuelle
    elif "slower" in result :                                                                               #si on a entendu le mot "slower"
        commande = change_vit(command,-0.5)                                                                 #On lance la fonction change_vit pour modifier la vitesse actuelle  
    elif "change" in result or "set" in result :                                                            #si on a entendu le mot "change" ou "set"
        if "speed" in result :                                                                              #si on a entendu le mot "speed", on pourra ajouter d'autre if plus tard s'il faut modifier d'autres variables
            multi = w2n.word_to_num(result)                                                                 #On récupère la valeur de la vitesse entendu
            commande = set_vit(command,multi)                                                               #On lance la fonction set_vit pour changer complètement la vitesse actuelle

    if commande != "":                                                                                      #On vérifie que la commande modifié n'est pas vide 
        global COMP                                                                                         #On mondifie la valeur global de COMP 
        COMP = 1                                                                                            #On donne a COMP la valeur 1, on a compris la commande mais il ne faut pas mettre à 2 pour ne pas stopper l'écoute
        os.system("export AUDIODEV="+device_name+"&&"+"export AUDIODRIVER=alsa"+"&&"+"play ./son/comp.wav -q")    #On joue le son voulu
        os.system("cd /tiago_public_ws")                                                                    #On retourne dans le bon repertoire si besoin
        print(commande)                                                                                     #On affiche la commande modifiée
        quick_stop()                                                                                        #On appele la fonction quick_stop pour être sur que le robot n'est pas déjà entrain d'avancer au moment où on lance la commande 
        os.system(commande)                                                                                 #On execute la commande
    else :                                                                                                  #Si la commande modifiée a toujours pour valeur sa valeur d'initialisation
        print("\033[0;91mla commande n'est pas modifiée\033[0m")                                            #On affiche un message signifiant que la commande n'a pas été modifiée

#-Stop--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
def stop():#Arreter le robot                                                                                    | 
    print("stop")                                                                                           #On affiche "stop" pour indiquer le début de la fonction  
    num = subprocess.check_output("ps aux | grep rostopic | cut -b 10-16", shell=True)                      #On cherche le numero du processus qui correspond au mouvement du robot
    num = convertion(num)                                                                                   #On modifie le resultat trouvé par le commande ci dessus pour avoir un nombre utilisable
    comm = "kill " + str(num)                                                                               #On créer la commande pour tuer le processus voulu
    #print("kill " + num)                                                                                   #On affiche cette commande
    os.system("export AUDIODEV="+device_name+"&&"+"export AUDIODRIVER=alsa"+"&&"+"play ./son/stop.wav -q")        #On joue le son voulu
    os.system(comm)                                                                                         #On execute cette commande
    thread_stop_tot = Thread(target=stop_tot)                                                               #On initialise le thread pour lancer la commande tot
    thread_stop_tot.start()                                                                                 #On lance le thread
    end_stop()                                                                                              #On lance la fonction end_stop pour arrêter le thread ci dessus
    thread_stop_tot.join()                                                                                  #On attend que le thread ne soit plus actif

def end_stop():#Arreter la fonctino stop                                                                        | 
    os.system("sleep 1")                                                                  #On attend 1 seconde que la fonction stop finisse d'excuter ce qu'elle devait executer
    num = subprocess.check_output("ps aux | grep rostopic | cut -b 10-16", shell=True)    #On cherche le numero du processus qui correspond au mouvement du robot
    num = convertion(num)                                                                 #On modifie le resultat trouvé par le commande ci dessus pour avoir un nombre utilisable
    comm = "kill " + str(num)                                                             #On créer la commande pour tuer le processus voulu
    #print("kill " + num)                                                                 #On affiche cette commande
    os.system(comm)                                                                       #On execute cette commande

def stop_tot():#Arrëter tout mouvement du robot                                                                 | 
    #print(tot)      #On affiche la commande "tot" qui arrête tout mouvement
    os.system(tot)   #Executer la commande tot, soit arrêter tout mouvement du robot

def quick_stop() :#Arreter le robot plus rapidement sans faire de bruit                                         |
    print("quick stop")                                                                                     #On affiche "stop" pour indiquer le début de la fonction  
    num = subprocess.check_output("ps aux | grep rostopic | cut -b 10-16", shell=True)                      #On cherche le numero du processus qui correspond au mouvement du robot
    num = convertion(num)                                                                                   #On modifie le resultat trouvé par le commande ci dessus pour avoir un nombre utilisable
    comm = "kill " + str(num)                                                                               #On créer la commande pour tuer le processus voulu
    os.system(comm)                                                                                         #On execute cette commande

#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
#-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
if __name__ == '__main__':#La fonction main                                                                     | 
    mic = pyaudio.PyAudio()                                                                                                             #On récupère les devices disponibles
    device_name = find_name(mic)                                                                                                        #On lance la fonction find_name pour trouver le nom du device que l'on va utiliser
    model = Model(r"./vosk-model-small-en-us-0.15")                                                   #On donne le chemin du model vosk qu'on va utiliser pour la reconnaisance vocal
    Recognizer=KaldiRecognizer(model,SAMPLE_RATE)                                                                                       #On trouve le model utilisé grace au chemin et on precise le sample rate
    
    #rendre l'audio dispo
    command = ["ffmpeg", "-loglevel", "quiet","-f", "alsa","-i", device_name,"-ar", str(SAMPLE_RATE) , "-ac", "1", "-f", "s16le", "-"]  #On écrit la commande qui va servire a lancer l'écoute
    while True :                                                                                                                        #On continue a faire tourner la boucle tant que le programme n'est pas coupé dans le terminal
        COMP = -1                                                                                                                       #On donne la valeur -1 a COMP pour le réinitialiser
        with subprocess.Popen(command,stdout=subprocess.PIPE) as process1 :                                                             #On lance l'écoute, on chercher à entendre les mots "hello robot" ou "stop"
            os.system("export AUDIODEV="+device_name+"&&"+"export AUDIODRIVER=alsa"+"&&"+"play ./son/debut.wav -q")                           #On joue le son voulu
            print("\033[0;95m \nEn écoute pour le WuW\033[0m")                                                                          #On affiche "en écoute pour le Wuw" pour signifier a l'utilisateur que l'écoute a bien commencé
            while True :                                                                                                                #On continue a faire tourner la boucle tant que on ne rentre pas dans les cas qui arrête la boucle, dans le cas où on a compris un Wake up Word
                result = ""                                                                                                             #On réinitialise le resultat à une chaine de caractères vide
                data = ""                                                                                                               #On réinitialise les données brutes à une chaine de caractères vide
                data = process1.stdout.read(4000)                                                                                       #On on récupére les données brutes de ce qui a été dit

                if Recognizer.AcceptWaveform(data):                                                                                     #Si les données brutes sont des données qui peuvent données des mots
                    text = Recognizer.Result()                                                                                          #On récupère ce qu'on entend
                    result = text[14:-3]                                                                                                #On retire la mise en forme de vosk pour n'avoir que le text dit pas l'utilisateur
                    print("j'ai entendu : \033[0;92m"+result+"\033[0m")                                                                 #On affiche ce que a entendu et retranscrit le model

                if "hello you" in result:                                                                                               #Si on a entendu les mots "hello you"                              
                    #print("j'ai entendu : \033[0;92m"+result+"\033[0m")                                                                #On affiche ce que a entendu et retranscrit le model
                    WW = "hello you"                                                                                                    #On donne au Wake up Word la valeur "hello you"
                    break                                                                                                               #On arrête la boucle while, on arrête l'écoute pour le wake up word

                elif "stop" in result:                                                                                                  #Si on a entendu le mot "stop"  
                    WW = "stop"                                                                                                         #On donne au Wake up Word la valeur "stop"
                    #print("j'ai entendu : \033[0;92m"+result+"\033[0m")                                                                #On affiche ce que a entendu et retranscrit le model
                    break                                                                                                               #On arrête la boucle while, on arrête l'écoute pour le wake up word
        process1.terminate()

        #WuW reconnu
        if WW == "hello you" :                                                                                                          #Si le wake up word est "hello you"
            print("\033[0;95m\nVous pouvez parler: \033[0m")                                                                            #On affiche "vous pouvre parler", on indique à l'utilisateur qu'il peut commencer à parler
            os.system("export AUDIODEV="+device_name+"&&"+"export AUDIODRIVER=alsa"+"&&"+"play ./son/WuW.wav -q")                             #On joue le son voulu
            i = 0                                                                                                                       #On initialise i à 0
            result = ""                                                                                                                 #On réinitialise le resultat à une chaine de caractères vide
            thread1=Thread()                                                                                                            #On initialise thread1 à un thread vide
            thread2=Thread()                                                                                                            #On initialise thread2 à un thread vide
            first = False                                                                                                               #On initialise first a False, On a encore jamais compris de commande
            f = True                                                                                                                    #On initialise f à True, on a encore rien entendu qui ressemble a des mots
            with subprocess.Popen(command,stdout=subprocess.PIPE) as process :                                                          #On lance l'écoute, on chercher à entendre les mots qui permettent de créer une commande
                while True:                                                                                                             #On continue a faire tourner la boucle tant que on ne rentre pas dans les cas qui arrête la boucle, dans le cas où une commande a pris fin
                    result = ""                                                                                                         #On réinitialise result
                    data = ""                                                                                                           #On réinitialise data
                    data = process.stdout.read(4000)                                                                                    #On on récupére les données brutes de ce qui a été dit

                    if Recognizer.AcceptWaveform(data):                                                                                 #Si les données brutes sont des données qui peuvent données des mots
                        text = Recognizer.Result()                                                                                      #On récupère ce qu'on entend
                        result = text[14:-3]                                                                                            #On retire la mise en forme de vosk pour n'avoir que le text dit pas l'utilisateur
                        print("j'ai entendu : \033[0;92m"+result+"\033[0m")                                                             #On affiche ce que a entendu et retranscrit le model
                        f = True                                                                                                        #On donne a f la valeur True, on a entendu quelque chose qui pourrait être des mots

                    if "stop" in result :                                                                                               #Si on a entendu le mot "stop"
                        print("\033[0;91mSTOP\033[0m")                                                                                  #On affiche "stop"
                        stop()                                                                                                          #On lance la fonction stop
                        break                                                                                                           #On arrête la boucle, on arrête d'écouter pour les commandes et on recommence a écouter pour les Wake up Word

                    if result !="":                                                                                                     #Si on a entendu quelque chose
                        COMP = 0
                        os.system("export AUDIODEV="+device_name+"&&"+"export AUDIODRIVER=alsa"+"&&"+"play ./son/fin_ecoute.wav -q")          #On joue le son voulu
                        if verife_dico(result,C) :                                                                                  #Si la fonction verife_dico renvoie True, si parmis les mots entendus on retrouve des mots servant a créer une commande
                            COMP = 1                                                                                                #On donne à COMP la valeur 1, on a compris une partie de la commande
                            print("COMP 2 : ", COMP)                                                                                                
                            #print("on a compris un bout")                                                                          #On affiche un message pour que l'utilisateur sache qu'un partie de la commande est compris
                        if COMP == 1 and not verife_dico(result,C2):                                                                #Si on a compris une partie de ce que l'utilisateur à dit
                            #print("on traite la demande")                                                                          #On affiche un message pour que l'utilisateur sache que ce qu'il a dit va être traité par le programme 
                            first = True                                                                                            #On donne à first la valeur True, On a lancé la fonction commander une première fois
                            thread1 = Thread(target=commander, args=[result,commande])                                              #On initialise le thread pour commander le robot
                            thread1.start()                                                                                         #On lance le thread 
                            result=""                                                                                               #On réinitialise result puisqu'il à déjà été traité                                                                                               
                            #print("on a compris un bout")  
                                              
                        if (thread1.is_alive() or thread2.is_alive()) and commande !="" and result!="" and verife_dico(result,C2) :     #Si thread1 ou thread2 est actif et la commande et result ne sont pas vides, si une commande est en cours et que l'utilisateur a dit quelque chose d'autre
                            COMP = 1                                                                                                    #On donne à COMP la valeur 1, on a compris une partie de la commande                            
                            thread2 = Thread(target=complement, args=[result,commande])                                                 #On initialise le thread pour modifier la commande du robot
                            thread2.start()                                                                                             #On lance le thread 

                    if (not thread1.is_alive() and first) and f :                                                                       #Si thread1 n'est pas actif et qu'il à déjà été activé, ou que c'est la première fois qu'on entend quelque chose 
                        print("on a traite la commande")                                                                                #On affiche un message pour que l'utilisateur sache que ce qu'il a dit à été traité par le programme                     
                        i = i + comp(COMP)                                                                                              #On lance la fonction COMP et on récupère la valeur qu'elle renvoie, on joue le son approprié et modifie la valeur de i pour savoir s'il faut continuer à écouter ou non, on continue a écouté sauf si ca fait 2 fois qu'on ne comprend rien ou si on a fini d'éxécuter une commande
                        print("on a compris ? "+str(COMP))                                                                              #On affiche un message pour que l'utilisateur connaisse la valeur de COMP                      
                        f = False                                                                                                       #On donne a f la valeur False, pour ne pas re rentrer dans le if aant d'avoir compris quelque chose
                    if i > 1:                                                                                                           #Si i est plus grand que 1, si on a pas compris du tout deux fois de suite ou si on a fini d'éxécuter la commande
                        stop()                                                                                                          #On appele la fonction stop pour être sur que le robot s'arrête avant d'écouter pour le wake up word
                        break                                                                                                           #On sort de la boucle

        if WW == "stop":                         #Le wake up word est "stop"
            print("\033[0;91mSTOP\033[0m")       #On affiche "stop" pour signier que le robot va se stoper
            stop()                               #On lance la fonction stop
